<!doctype html>
<html lang="{{ app()->getLocale() }}" class="h-100 w-100">
    <head>
        <!-- Required meta tags -->
        <meta charset="{{ config('app.charset') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="{{ config('app.viewport') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="description" content="{{ config('app.description') }}">
        <meta name="keywords" content="{{ config('app.keywords') }}">
        <meta name="author" content="{{ config('app.author') }}">

        <title>@yield('page-title'){{ '  |  '.config('app.name').' - '.config('app.version') }}</title>

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/theme.css') }}" />
        @stack('styles')
        <!-- CSS -->
    </head>
    <body class="h-100 w-100">
        <div id="custom-app" class="h-100 w-100">

            @include('layouts.nav')

            <main class="container-fluid mb-5">
                <div class="card border-0 shadow-sm mx-4">
                    <div class="card-body p-4">

                        @yield('main-content')

                    </div>
                </div>
            </main>
        </div>

        <!-- scripts -->
        <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
        @stack('scripts')
        <!-- scripts -->
    </body>
</html>