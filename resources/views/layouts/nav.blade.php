<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-secondary sticky-top mb-5 shadow-sm border-bottom">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('landing') }}">
            <img src="https://getbootstrap.com/docs/4.1/assets/brand/bootstrap-solid.svg" width="30" height="30">
            <span class="text-uppercase">{{ config('app.name') }}</span> <small class="text-white-50">{{ config('app.version') }}</small>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link text-uppercase" href="{{ route('landing') }}">
                        <i class="fas fa-chart-line btn-sm"></i> Dashboard
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-uppercase" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-users btn-sm"></i> Patients
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">New</a>
                        <a class="dropdown-item" href="#">Search</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Ledger</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Ledger history</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">
                        <i class="fas fa-file-invoice-dollar btn-sm"></i> Billing
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">
                        <i class="fas fa-file-alt btn-sm"></i> Reports
                    </a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                @if (Route::has('login'))
                    @if (!Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/login') }}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/register') }}">Register</a>
                </li>
                    @endif
                @endif
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-uppercase" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-clinic-medical btn-sm"></i> Practice Setup
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">New</a>
                        <a class="dropdown-item" href="#">Search</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Ledger</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Ledger history</a>
                    </div>
                </li>
            </ul>

        </div>
    </div>
</nav>